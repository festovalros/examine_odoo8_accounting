# Modelos de account

**nota**: todas las clases en este documento estan identificadas con su atributo `_name`, algunas clases de este modelo no tienen este atributo, en esos casos se indentificaran por el nombre de la clase, que se diferencian del indentificador `_name` porque en sus nombre se usa underline como separador de palabras y no punto como en el identificador.
___

## Modelo account.py
Este modelo es uno de los que contiene mayor cantidad de lineas de codigo, si se puede decir que el modulo account es la base de toda la logica contable de odoo, tambien podriamos decir que este modelo es la base de la logica modulo.

a continuación las clases que componen este modelo y sus caracteristicas:

### res_company
Cuando esta activada las opciones multimoneda de la compañia los campos definidos en esta clase determinan las cuentas contables donde se registran los montos en moneda extranjera.

### account.payment.term
esta clase provee los campos para definir los terminos de pago aceptado por la compañia y computa los montos de los pagos dependiendo de las condiciones del pago.

### account.payment.term.line
estas son las lineas de los terminos de pago cuando se crea un nuevo termino de pago desde Accounting > Settings > Miscellaneous > Payment terms.

### account.account.type
esta clase crea los reportes financieros filtrando la informacion que muestra por tipos de cuentas contables, este tipo de cuenta es diferente al tipo de cuenta interno, el tipo de cuenta interno define la naturaleza contable de la cuenta, el tipo de cuenta a secas es como una etiqueta con que se carga la informacion en los informes financieros tambien. Esta clasificacion en contabilidad es llamada centro de costos.

### account.account
esta clase define todos los campos que definen una cuenta contable y el computo de los montos en los debitos y creditos y el saldo.

### account.journal
Esta clase define los campos que conforman los diarios contables sus naturalezas segun el tipo de diario y sus correspondencias con las cuentas contables.

### account.fiscalyear
esta clase define el año del ejercicio fiscal y la division de los periodos contables.

### account.period
esta clase define los campos de los periodos fiscales.

### account.move
esta clase define los campos requeridos para hacer un movimiento contable (asiento) trae toda la informacion fiscal y de facturacion necesaria y computa los balancer de las cuentas y realiza los asientos en los diarios correspondientes.

### account.move.reconcile
esta clase genera movimientos contables via reconcilizacion de pagos, es decir sin necesidad de hacer un asiento manualmente.

### account.tax.code
esta Clase define los codigos de impuestos son como categorias que agrupan varios impuestos que comparten una misma naturaleza.

### account.tax
Esta clase define los campos de los impuestos propiamente dichos.el computo de sus valores y cuando deben ser aplicados y como se prensentaran en la factura.

### account.model
esta clase permite elaborar plantillas de asientos recurrentes.

### account.model.line
define las lineas para la plantilla de de los asientos recurrentes.

### account.subscription
Esta clase define los campos para hacer un asiento recurrente especifico.

### account.subscription.line
las lineas de los asientos recurrentes especificos.     

las siguientes clases son plantillas de cuentas, impuestos, codigos de impuestos y planes de cuentas y asistentes(wizards) que permite la instalacion de todas estas plantillas.

### account.account.template
 clase que define plantillas para la creacion de cuentas contables
   
### account.addtmpl.wizard
wizard para crear cuentas contables a partir de plantillas

### account.tax.code.template
 plantilla para codigos de iimpuestos


### account.chart.template
Esta define la plantilla para crear arboles de cuenta o planes contables completos.
 
### account.tax.template
plantillas para impuestos especificos

### account.fiscal.position.template
Plantillas para posiciones fiscales 

### account.fiscal.position.tax.template
Plantillas para impuestos de posicion fiscal

### account.fiscal.position.account.template
plantilla de cuentas para posicion fiscal

### wizard.multi.charts.accounts
 wizard que hace un deploy de un plan de cuentas contables.

### account.bank.accounts.wizard
wizard para configurar los cuentas contables de los bancos registrados.

___

## Modelo account_analytic_line.py
Este modelo define la interaccion de la cuentas analiticas (cuya logica de negocio se encuentra en el modulo analytic) con las cuentas contables financieras. Sus clases son:

### account_analytic_line
Esta clase es heredada de la clase `account.analytic.line` que se encuentra en el modelo analytic.py del modulo analytic, esta clase define los campos de las lineas de movimientos contables en cuentas analiticas y la informacion que se trae de los campos de otos modulos para generar estas lineas.

### res_partner
clase que hereda `res.partner` y agrega el campo de contratos.

___

## Modelo account_bank.py
Modelo con una sola clase.
## bank
Esta clase heredada de `res.partner bank` crea el diario bancario cuando una cuenta bancaria es registrada en el sistema.

___

## Modelo account_bank_statement.py

### account.bank.statement
Esta clase define todas los campos de los extractos bancarios y el calculo del balance inicial y final que se computa con los montos de las lineas del extracto. Determina a que diario contable van las lineas del extrato y a que periodo del año fiscal pertenece, verifica que estos periodos correspondan con las fechas de las entradas. Computa los argumentos necesarios para realizar los movimientos en cuentas contables una vez el extracto se haya conciliado. Computa que el balance sea correcto y define el workflow de las etapas del extracto. Tambien estan los metodos para desvincular extractos cuando ya han sido cargados a los diarios.

### account.bank.statement.line"
Esta clase tiene los metodos que definen las relaciones entre cada linea del extracto bancario y el extracto como tal. Tambien contiene los metodos necesarios para extraer la data requerida de cada linea para la concilizacion contra los montos de las facturas y realizar los movimientos de cuentas contables.

## Modelo account_cash_statement.py
Modelo que define la logica de negocio de las cajas de efectivo, contiene las siguientes clases:

### account.cashbox.line
Esta clase define los campos y los metodos de calculo de las cajas de efectivo.

### account_cash_statement 
Esta clase hace practicamente lo mismo que la clase `account.bank.statement`(de la cual hereda) pero en este caso solo computa la data que se agrega como efectivo en el modulo de caja. Ademas contiene los metodos de workflow propio de  una caja de efectivo, abrir, cerrar, computar diferencias etc.

Esta dos ultimas  clases se encargan de cargar las lineas de extracto de efectivo en un diario contable de tipo efectivo:

### account_journal
heredada de `account.journal`
### account.journal.cashbox.line
___

## Modelo account.financial.report.py
modelo con una sola clase

### account.financial.report

Esta clase contiene los metodos que definen los calculos que se hacen en el reporte financiero que es simplemente un informe con los balances generales de grupos de cuentas contables definidos por el tipo (centro de costos) de las cuentas.
---

## modelo account_invoice.py

Este modelo define toda la logica de negocios de la facturación y toda la estructura de las facturas al principio del modelo encontramos dos diccionarios de nombre TYPE2JOURNAL y TYPE2REFUND definen la correspondencia entre el tipo de factura y el diario contable donde seran asentadas dichas facturas para las facturas validadas y las que necesitan ser devueltas respectivamente. A continuación las clases que componen el modelo:

### account.invoice
Esta clase crea todos los campos y relaciones de datos que lleva la factura computa los valores totales de las lineas de factura genera los movimientos en las cuentas contables, define el workflow de las facturas, computa los residuos cotnra los pagos y las reconciliaciones crea facturas a partir de documentos de compras y ventas o de movimientos contables o financieros, permite la cancelacion de facturas y que sea devueltas al estaddo borrador.

### account.invoice.line
Esta clase define los campos de las lineas de facturacion y sus relaciones con productos y partners, los impuestos y las posiciones fiscales asociadas.

### account.invoice.tax
Esta clase calcula los impuestos segun las bases imponibles que les correspondan y genera lso movimientos en las cuentas contables correspondientes.

### res_partner
clase heredada `res.partner` que agrega la informacion de facturacion en los campos de rest.partner.

### mail_compose_message
clase que hereda `mail.compose.messaje` y controla el envio de las facturas por correo electronico.

___

## Modelo account_move_line

modelo con una sola clase pero bastante extensa con varios metodos que se encarga de hacer los movimientos contables en el arbol de cuentas su unica clase es la siguiente:

### account.move.line
esta clase define los movimientos de las cuentas contables, calcula los montos residuales de las cuentas por pagar o por cobrar revisa los montos iniciales de las cuentas, prepara los valores de las cuentas analiticas para hacer los movimientos en las contables propone los montosy en que cuentas hacer los proximos movimientos en debitos/creditos para balancear el movimiento propone soluciones automaticas cuando se presentan incosistencias en los balances de las cuentas computa los montos de los movimientos entre las cuentas cuando son en multi moneda con la tasa de cambio que tenga definida el sistema o una definida en el momento extrae toda la informacion necesaria de las facturas y las reconciliaciones para realizar el movimiento contable. Establece las restricciones necesarias para que los movimientos esten en los periodos fiscales y los diarios correspondientes a cada tipo de movimiento o eventos tipo que no se pueden crear entradas en diarios desde cuentas marcadas como cerradas. Actualiza los diarios segun los movimientos de cuentas que se hayan realizado

___

## Modelo company.py
modelo con una sola clase:

### res_company
Esta clase define los impuestos que aplican para la empresa y en que cuentas contables se realizaran los movimientos de estos impuestos.
___
## Modelo installer.py

Modelo que es un asistente para arrancar las configuraciones basicas del modulo contable con solo una clase:

### account.installer
Esta clase es un wizard que ayuda a configurar el modulo contable al instalar, definiendo un periodo fiscal y un arbol de cuentas contables y la compañia a la cual se le aplicara el plan contable.
___

## Modelo ir_sequence.py

Este modelo tiene dos clases que permiten usar como sencuencias la informacion referente de los periodos del año fiscal.

### account.sequence.fiscalyear
### ir_sequence

___

## Modelo partner.py

modulo con 4 clases que definen caracteristicas contables especiales para los partner las clases son:

### account.fiscal.position
Esta clase define las **posiciones fiscales** que son un conjunto de caracteristicas particulares de un partner, por ejemplo si es un cliente de otro pais, los impuestos y las cuentas contables que se apliquen sobre el son distintas a las de un cliente del mismo pais d ela compañia, la posicion fiscal es como la agrupacion de todas estas caracteristicas que por lo tando se le pueden aplicar en conjunto a partner que sean provenientes del mismo pais. Esta clase define posiciones fiscales que posteriormente con  la ultima clase de este modelo se le aplica a un partner en particular.

### account.fiscal.position.tax
define los impuestos dentro de la posicion fiscal particular.

### account.fiscal.position.account
Define las cuentas contables para posiciones fiscales particulares.

### res_partner

Clase heredada de `res.partner` que asigna las posiciones fiscales a los partner que se deseen.
___

## Modelo product.py

Este modelo tiene dos clases que definen las cuentas contables por defecto de los productos:

### product_category

Clase heredada de `product.category`agrega la informacion de las cuentas contables por defecto para categorias de productos.

### product_template

Clase heredada de `product.template` agrega las cuentas contables por defecto para los productos individualmente.

___

## Modelo res_config

Cuenta con una clase que define la contabilidad para una compañia que este siendo administrada por el sistema.

### account.config.settings
Esta clase permite configurar todos los componentes contables para una compañia, como la posicion fical, impuestos por defecto, arbol de cuentas, periodos fiscales y tasas de cambio para operaciones multi-moneda.

___

## Modelo res_currency.py
Cuenta con una clase para la tasas de cambios de divisas.
### res_currency_account
Esta clase maneja los que valores de tasa de cambio cuando se necesitan hacer conversiones en la cuentas contables, segun el momento que se genere la data.
