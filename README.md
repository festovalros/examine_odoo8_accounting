# Examine_odoo8_accounting
inspección y documentación del módulo contable de odoo 8
___
# Convenciones
En este Documento se expondrá las funciones generales de los módulos de contabilidad de odoo 8, a su vez en cada carpeta de cada modulo habra un archivo md con la siguiente estructura de nombre: `<nombre_del_Modulo-Componente_del_modulo.md>` en donde se tratara de documentar a profundidad la funcionalidad de los componentes de cada modulo contable, por ejemplo para los modelos del modulo `account` el archivo seria: `account-models.md`, para las vistas del modulo account el archivo seria: `account-views.md`, y asi sucesivamente para el resto de los componentes. En algunos casos tambien existira intradocumentcion con comentarios dentro de los archivos de los modulos propiamente, estos comentarios estaran referenciados en los archivos md ya mencionados.

# Funcionalidad general de los Modulos Contables.

## account
Este modulo agrega la funcionalidad de gestion de facturas tanto de compras como de ventas, gestiona los tipos de pagos y permite su automatizacion basadas en las facturas de proveedor, gestiona caracteristicas de pagos electronicos por vias como paypal, lleva el registro financiero de los pagos tanto como agregar pagos manualmente o por reconcilizacion bancaria de las facturas contra los estados bancarios que emitan los bancos, tambien emite informes finacieros basicos de compras y ventas.

## account_accountant
Este modulo otorga los permisos necesarios para que al administrador contable pueda ver y modificar caracteristias contables como los diarios, apuntes contables, plan de cuentas, asientos etc., Este modulo no tiene cuenta con modelos, solo vistas y reglas de acceso. 

## account_analytic_analysis
Este modulo agrega un nueva vista a los reportes de contabilidad donde se puede visualizar informacion relevante de las cuentas contables analiticas. El menu se encuentra en la ruta `Reporting > Accounting > Analytic Entries Analysis`.

## account_analytic_default
Este modulo permite crear cuentas analiticas de forma automatica segun los criterios

+ Product
+ Partner
+ User
+ Company
+ Date

## account_analytic_plans
Este modulo permite crear planes de cuentas analiticas basados en los diarios generales de forma que al confirmar una factura este modulo tiene la capacidad de crear lineas de entrada en las cuentas analiticas automaticamente cuando se valida una factura y de acuerdo a como este estructurado el plan de cuentas analitico.

## account_asset
Este modulo proporciona la gestion de activos de una empresa o persona, añade reglas de depreciacion y crea entradas en los diarios correspondientes. Este modulo activa el menu `Accounting > Assets`, si el la casilla de manejos de activos esta activada en la configuracion contable del sistema.

## account_bank_statement_extensions
este modulo extiende las funcionalidades del manejo de estados bancarios con las siguientes caracteristicas:

+ fecha efectiva de transferencias o depositos.
+ pagos en lotes
+ traceabilidad de cambios en lineas de estado de cuentas bancarias
+ Vistas de las lineas de estado de cuenta bancaria
+ Reporte de balance de estados de cuenta.
Mejora en la importacion de estados de cuenta banarios.
+ Campo de busqueda mejorado que permite buscar en buscar bancos y numeros de cuenta IBAN.


## account_budget
Este modulo permite la creacion de presupuestos y definir montos planeados en cuentas analiticas. Agrega el menu `Accounting > Budgets > Budgets`.

## account_cancel
Esto modulo agrega una accion que permite borrar entradas de asientos en diarios contables o facturas validadas.

## account_chart
Agrega plantillas de planes de cuenta minimos.

## account_check_writing
Este modulo ofrece las funcionalidades basicas para imprimir cheques directamente desde el sistema, es usado como dependencia para modulos de localizaciones especificas que contienen las plantillas de cheques segun el pais.

## account_followup
Este modulo agrega la funcionalidad de seguimiento de pagos, activando la casilla de `Manage customer payment follow-ups` en `Settings > Accounting` y y en el boton continuo de `Configure your follow-up levels` podemos configurar los recordartorios de pagos atrasados por ventas que se mandaran automaticamente a los clientes que no hayan pagado la facturas, ademas de recordatorios para los usuarios del sistema de los pagos no realizados, tambien agrega el reporte de seguimiento de pagos en `Reporting > Accounting > Follow-ups Analysis`.
 
## account_payment
Este modulo permite el manejo de los pagos a proveedores, permite generar ordenes de compras y automatizar pagos recurrentes a proveedores, este modulo solo sirve para llevar un registro de los pagos pero no genera ninguna entrada contable, para que los pagos sean efectivos del lado de la contabilidad tienen que ser primeros exportados a los extractos cambiarios y de ahi si se reflejan en los planes de cuentas financieros.

## account_sequence
Permite modificar las secuencias internas de las entradas contables, con este modulo se puede personalizar los siguientes atributos de las secuencias:
+ prefijos
+ Sufijos
+ Numero Siguiente 
+ Numero Incremental
+ Digitos de Relleno 

## account_test

## account_voucher
Este modulo permite la creacion de recibos de pagos y ventas y de entradas de asientos contables a los diarios para llevar un control adicional a la facturacion, o para utilizarla cuando no se este usando la facturacion o toda la logica contable pero se quiera llevar un registro de los pagos.

## analytic
Este modulo crea toda la logica de cuentas analiticas en el sistema, se pueden realizar diferentes operaciones en cuentas contables analiticas sin tocar la contraparte de cuentas financieras o relacionar cuentas analiticas automaticamente con cuentas financieras.
